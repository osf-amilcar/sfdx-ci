#!groovy
import groovy.json.JsonSlurperClassic

node {
    println '### DELIVERY AUTOMATION STARTED ###';

    def BUILD_NUMBER=env.BUILD_NUMBER
    def RUN_ARTIFACT_DIR="tests/${BUILD_NUMBER}"
    def HUB_ORG=env.HUB_ORG_DH
    def HOMOLOGATION_ORG=env.HOM_ORG_DH
    def HOM_ORG_PASS_ID = env.HOM_ORG_PASS_ID_DH
    def SFDC_HOST = env.SFDC_HOST_DH
    def JWT_KEY_CRED_ID = env.JWT_CRED_ID_DH
    def CONNECTED_APP_CONSUMER_KEY=env.CONNECTED_APP_CONSUMER_KEY_DH
    def toolbelt = tool 'toolbelt'

    stage('checkout source') {
        // when running in multi-branch job, one must issue this command
        checkout scm
    }

    withCredentials([
        file(credentialsId: JWT_KEY_CRED_ID, variable: 'jwt_key_file'),
        string(credentialsId: HOM_ORG_PASS_ID, variable: 'hom_org_installation_key')
    ]) {
        stage('Authorize Dev Hub Org') {
            if(isUnix()) {
                rc = sh returnStatus: true, script: "${toolbelt} force:auth:jwt:grant --clientid ${CONNECTED_APP_CONSUMER_KEY} --username ${HUB_ORG} --jwtkeyfile ${jwt_key_file} --setdefaultdevhubusername --instanceurl ${SFDC_HOST}"
            } else {
                rc = bat returnStatus: true, script: "\"${toolbelt}\" force:auth:jwt:grant --clientid ${CONNECTED_APP_CONSUMER_KEY} --username ${HUB_ORG} --jwtkeyfile \"${jwt_key_file}\" --setdefaultdevhubusername --instanceurl ${SFDC_HOST}"
            }

            if (rc != 0) {
                error 'hub org authorization failed'
            }
        }

        stage('Deliver Last Package Version') {
			timeout(time: 120, unit: 'SECONDS') {
                if(isUnix()) {
                    rc = sh returnStdout: true, script: "${toolbelt} force:package:version:list --json"
                } else {
                    rc = bat returnStdout: true, script: "\"${toolbelt}\" force:package:version:list --json"
                }

                def beginIndex = rc.indexOf('{')
                def jsonSubstring = rc.substring(beginIndex)                
                def jsonSlurper = new JsonSlurperClassic()
                def robj = jsonSlurper.parseText(jsonSubstring)
                def status = robj.status
				
				if (status != 0) {
					error 'package version list request failed'
				}

                def versions = robj.result
                def numberVersions = versions.size()
                def lastVersion = versions.getAt(numberVersions - 1)
                println("lastVersion")
                println(lastVersion)

                def packageName = lastVersion.Package2Name
                def majorVersion = lastVersion.MajorVersion
                def minorVersion = lastVersion.MinorVersion
                def patchVersion = lastVersion.PatchVersion
                def buildNumber = lastVersion.BuildNumber
                def versionConvention = "${packageName}@${majorVersion}.${minorVersion}.${patchVersion}-${buildNumber}"

                // if(isUnix()) {
                //     rc = sh returnStatus: true, script: "${toolbelt} force:package:install -w 10 -b 10 -p ${versionConvention} -k ${hom_org_installation_key} -r -u ${HOMOLOGATION_ORG}"
                // } else {
                //     rc = bat returnStatus: true, script: "\"${toolbelt}\" force:package:install -w 10 -b 10 -p ${versionConvention} -k ${hom_org_installation_key} -r -u ${HOMOLOGATION_ORG}"
                // }

                // if (rc != 0) {
                //     error 'package version installation failed'
                // }
			}
		}
    }
}
